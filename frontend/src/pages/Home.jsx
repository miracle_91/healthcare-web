import heroImg01 from "../assets/images/hero-img01.png";
import heroImg02 from "../assets/images/hero-img02.png";
import heroImg03 from "../assets/images/hero-img03.png";
import icon01 from "..//assets/images/icon01.png";
import icon02 from "..//assets/images/icon02.png";
import icon03 from "..//assets/images/icon03.png";
import { Link } from "react-router-dom";
import { BsArrowRight } from "react-icons/bs";
import faqImg from "../assets/images/faq-img.png";
import videoIcon from "../assets/images/video-icon.png";
import avatarIcon from "../assets/images/avatar-icon.png";
import Aboutus from "../components/Aboutus/Aboutus";
import ServiceList from "../components/Services/ServiceList";
import featureImage from "../assets/images/feature-img.png";
import DoctorList from "../components/Doctors/DoctorList";
import FaqList from "../components/Faq/FaqList";



const Home = () => {
  return (
    <>
      {/* ------------------Hero Sec------------- */}
      <>
        <section className="hero__section pt-[60px] 2xl:h-[800px]">
          <div className="container">
            <div className="flex flex-col lg:flex-row gap-[90px] items-center justify-between">
              {/* ------------------Hero Content------------- */}
              <div>
                <div className="lg:w-[570px]">
                  <h1 className="text-[36px] leading-[46px] text-headingColor font-[800] md:tex-[60px] md:leading-[70px]">
                    {" "}
                    We assist patients in leading healthier, longer lives.
                  </h1>
                  <p className="text__para">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Maxime aliquam modi, unde, iure amet neque ipsum omnis quo
                    voluptas, excepturi explicabo placeat? Obcaecati, voluptatum
                    animi! Velit dolorum quis eligendi? Amet.
                  </p>

                  <button className="btn"> Request an Appointment </button>
                </div>
                {/* ------------------Hero Counter------------- */}
                <div className="mt-[30px] lg:mt=[70px] flex flex-col lg:flex-row lg:items-center gap-5 lg:gap-[30px]">
                  <div>
                    <h2 className="text-[36px] leading-[56px] lg:text-[44px] lg:leading-[54px] font-[700] text-headingColor">
                      3+
                    </h2>
                    <span className="w-[100px] h-2 bg-yellowColor rounded-full block mt-[-14px]"></span>
                    <p className="text__para">Years of Experience</p>
                  </div>
                </div>
              </div>

              {/* ------------------Hero Content------------- */}
              <div className="flex gap-[30px] justify-end ">
                <div>
                  <img className="w-full" src={heroImg01} alt="" />
                </div>

                <div className="mt-[30px]">
                  <img src={heroImg02} alt="" className="w-full mb-[30px]" />
                  <img src={heroImg03} alt="" className="w-full " />
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* ------------------Hero End------------- */}

        <section>
          <div className="container">
            <div className="lg:w-[470px] mx-auto">
              <h2 className="heading text-center">
                Providing the best medical services
              </h2>
              <p className="text__para text-center">
                World-class treatment for everybody. Our Smart Healthcare System
                provides top-notch healthcare that is unrivaled.
              </p>
            </div>

            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-5 lg:gap-[30px] mt-[30px] lg:mt-[55px]">
              <div className="py-[30px] px-5">
                <div className="flex items-center justify-center">
                  <img src={icon01} alt="" />
                </div>

                <div className="mt-[30px]">
                  <h2 className="text-[26px] leading-9 text-headingColor font-[700] text-center">
                    Find Doctor
                  </h2>
                  <p className="text-[16px] leading-9 text-textColor font-[400] mt-4 text-center">
                    Excellent treatment for evervone. Our smart healthcare
                    system provides unmatched, professional medical treatment.
                  </p>

                  <Link
                    to="/doctors"
                    className="w-[44px] h-[44px] rounded-full border border-solid border-[#181A1E] mt-[30px] mx-auto flex items-center justify-center group hover:bg-primaryColor hover:border-none "
                  >
                    <BsArrowRight className="group-hover:text-white w-6 h-5" />
                  </Link>
                </div>
              </div>

              <div className="py-[30px] px-5">
                <div className="flex items-center justify-center">
                  <img src={icon02} alt="" />
                </div>

                <div className="mt-[30px]">
                  <h2 className="text-[26px] leading-9 text-headingColor font-[700] text-center">
                    Location
                  </h2>
                  <p className="text-[16px] leading-9 text-textColor font-[400] mt-4 text-center">
                    Excellent treatment for evervone. Our smart healthcare
                    system provides unmatched, professional medical treatment.
                  </p>

                  <Link
                    to="/doctors"
                    className="w-[44px] h-[44px] rounded-full border border-solid border-[#181A1E] mt-[30px] mx-auto flex items-center justify-center group hover:bg-primaryColor hover:border-none "
                  >
                    <BsArrowRight className="group-hover:text-white w-6 h-5" />
                  </Link>
                </div>
              </div>

              <div className="py-[30px] px-5">
                <div className="flex items-center justify-center">
                  <img src={icon03} alt="" />
                </div>

                <div className="mt-[30px]">
                  <h2 className="text-[26px] leading-9 text-headingColor font-[700] text-center">
                    Schedule
                  </h2>
                  <p className="text-[16px] leading-9 text-textColor font-[400] mt-4 text-center">
                    Excellent treatment for evervone. Our smart healthcare
                    system provides unmatched, professional medical treatment.
                  </p>

                  <Link
                    to="/doctors"
                    className="w-[44px] h-[44px] rounded-full border border-solid border-[#181A1E] mt-[30px] mx-auto flex items-center justify-center group hover:bg-primaryColor hover:border-none "
                  >
                    <BsArrowRight className="group-hover:text-white w-6 h-5" />
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </section>

        <Aboutus />

        {/* ------------------Services Page------------ */}
        <section>
          <div className="container">
            <div className="xl:w-[470px] mx-auto">
              <h2 className="heading text-center">Our Healthcare Services</h2>
              <p className="text__para text-center">
                We provide world-class healthcare for everyone through our Smart
                Healthcare System.
              </p>
            </div>

            <ServiceList />
          </div>
        </section>
        {/* ------------------Services End------------- */}

        {/* ------------------Feature ------------ */}
        <section>
          <div className="container">
            <div className="flex items-center justify-between flex-col lg:flex-row">
              {/* ------------------Feature content------------ */}
              <div className="xl:w-[670px]">
                <h2 className="heading">
                  Get virtual treatment <br /> anytime
                </h2>
                <ul className="pl-4">
                  <li className="text__para">
                    1. Schedule appointment directly
                  </li>
                  <li className="text__para">
                    2. Search your Doctor here, and contact their office
                  </li>
                  <li className="text__para">
                    3. View our Doctors who are accepting new patients,
                    <br /> use the online scheduling tool to select an
                    appointment
                  </li>
                </ul>

                <Link to="/">
                  <button className="btn">Learn More</button>
                </Link>
              </div>
              {/* ------------------Feature image------------ */}
              <div className="relative z-10 xl:w[770px] flex justify-end mt-[50px] lg:mt-0">
                <img src={featureImage} className="w-3/4" alt="" />

                <div className="w-[150px] lg:w-[248px] bg-white absolute bottom-12 left-0 md:bottom-[100px] md:left-5 z-20 p-2 pb-3 lg:pt-4 lg:px-4 lg:pb-[26px] rounded-[10px]">
                  <div className="flex items-center justify-between">
                    <div className="flex items-center gap-[6px] lg:gap-3">
                      <p className="text-[10px] leading-[10px] lg:text-[14px] lg:leading-5 text-headingColor font-[600]">
                        Mon, 17
                      </p>
                      <p className="text-[10px] leading-[10px] lg:text-[14px] lg:leading-5 text-textColor font-[400]">
                        8:00 AM
                      </p>
                    </div>

                    <span className="w-5 h-5 lg:w-[34px] lg:h-[34px] flex items-center justify-center bg-yellowColor rounded py-1 px-[6px] lg:px-[9px]">
                      <img src={videoIcon} alt="" />
                    </span>
                  </div>

                  <div className="w-[65px] lg:w-[96px] bg-[#CCF0F3] py-1 px-2 lg:py-[6px] lg:px-[10px] text-[8px] leading-[8px] lg:text-[12px] lg:leading-4 text-irisBlueColor font-[500] mt-2 lg:mt-4">
                    Consultation
                  </div>

                  <div className="flex items-center gap-[6px] lg:gap-[10px] mt-2 lg:mt-[18px]">
                    <img src={avatarIcon} alt="" />
                    <h4 className="text-[10px] leading-3 lg:text-[16px] lg:leading-[22px] font-[700] text-headingColor">
                      Doc Willie
                    </h4>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* ------------------Feature End------------ */}

        {/* ------------------Doctors------------ */}
        <section>
          <div className="container">
            <div className="xl:w-[470px] mx-auto">
              <h2 className="heading text-center">Our Doctors</h2>
              <p className="text__para text-center">
                We provide world-class healthcare for everyone through our Smart
                Healthcare System.
              </p>
            </div>

            <DoctorList />
          </div>
        </section>
        {/* ------------------Doctors End------------ */}

        {/* ------------------Faqs start------------ */}
        <section>
          <div className="container">
            <div className="flex justify-between gap-[50px] lg:gap-0">
              <div className="w-1/2 hidden md:block">
                <img src={faqImg} alt="" />
              </div>

              <div className="w-full md:w-1/2">
                <h2 className="heading">
                  Most question by our beloved patients
                </h2>
                <FaqList />
              </div>
            </div>
          </div>
        </section>
        {/* ------------------Faqs End------------ */}

        {/* ------------------testimonial start------------ */}
       
      </>
    </>
  );
};

export default Home;
