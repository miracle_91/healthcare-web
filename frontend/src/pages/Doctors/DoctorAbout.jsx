import { formateDate } from "../../utils/formateDate";

const DoctorAbout = () => {
  return (
    <div>
      <div>
        <h3 className="text-[20px] leading-[30px] text-headingColor font-semibold flex items-center gap-2">
          About of
          <span className="text-secondColor font-bold text-[24px] leading-9">
            Portgas D. Ace
          </span>
        </h3>
        <p className="text__para">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt,
          delectus provident. Earum eos blanditiis dolorem aliquid recusandae?
          Necessitatibus, unde iste animi nostrum fugiat doloremque quo
          deserunt! Veniam consequuntur voluptas facere suscipit reiciendis! Ut
          distinctio dolorem iste. Architecto, unde. Veritatis dolorem
          voluptates quos. Dicta, quis aut quia esse quod deserunt. Quo.
        </p>
      </div>

      <div className="mt-12">
        <h3 className="text-[20px] leading-[30px] text-headingColor font-semibold">
          Education
        </h3>

        <ul className="pt-4 md:p-5">
          <li className="flex flex-col sm:flex-row sm:justify-between sm:items-end md:gap-5 mb-[30px]">
            <div>
              <span className="text-secondColor text-[15px] leading-6 font-semibold">
                {formateDate("10-14-2015")} - {formateDate("6-11-2017")}
              </span>
              <p className="text-[16px] leading-6 font-medium text-textColor">
                PHD in Surgeon
              </p>
            </div>
            <p className="text-[14px] leading-5 font-medium text-textColor">
              R1 Hospital, Dagupan City
            </p>
          </li>

          <li className="flex flex-col sm:flex-row sm:justify-between sm:items-end md:gap-5 mb-[30px]">
            <div>
              <span className="text-secondColor text-[15px] leading-6 font-semibold">
                {formateDate("08-1-2011")} - {formateDate("10-14-2013")}
              </span>
              <p className="text-[16px] leading-6 font-medium text-textColor">
                PHD in Surgeon
              </p>
            </div>
            <p className="text-[14px] leading-5 font-medium text-textColor">
              R1 Hospital, Dagupan City
            </p>
          </li>
        </ul>
      </div>

      <div className="mt-12">
        <h3 className="text-[20px] leading-[30px] text-headingColor font-semibold">
          Experience
        </h3>

        <ul className="grid sm:grid-cols-2 gap-[30px] pt-4 md:p-5">
          <li className="p-4 rounded bg-[#fff9ea]">
            <span className="text-yellowColor text-[15px] leading-6 font-semibold">
              {formateDate("08-1-2011")} - {formateDate("10-14-2013")}
            </span>
            <p className="text-[16px] leading-6 font-medium text-textColor">
              Sr. Surgeon
            </p>

            <p className="text-[14px] leading-5 font-medium text-textColor">
              R1 Hospital, Dagupan City
            </p>
          </li>

          <li className="p-4 rounded bg-[#fff9ea]">
            <span className="text-yellowColor text-[15px] leading-6 font-semibold">
              {formateDate("08-1-2011")} - {formateDate("10-14-2013")}
            </span>
            <p className="text-[16px] leading-6 font-medium text-textColor">
              Sr. Surgeon
            </p>

            <p className="text-[14px] leading-5 font-medium text-textColor">
              R1 Hospital, Dagupan City
            </p>
          </li>
        </ul>

        
      </div>
    </div>
  );
};

export default DoctorAbout;
