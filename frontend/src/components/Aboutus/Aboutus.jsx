import AboutImg from '../../assets/images/about.png'
import AboutCardImg from '../../assets/images/about-card.png'
import {Link} from 'react-router-dom'


const Aboutus = () => {
  return <section>
    <div className="container">
        <div className="flex justify-between gap-[50px] lg:gap-[130px] xl:gap-0 flex-col lg:flex-row ">
            {/* ---------------About Img Start--------------*/}
        
        <div className="relative w-3/4 lg:w-1/2 xl:w-[770px] z-10 order-2 lg:order-1">
            <img src={AboutImg} alt="" />
            <div className="absolute z-20 bottom-4 w-[200px] md:w-[300px] right-[-30%] md:right-[7%] lg:right-[22%]">
                <img src={AboutCardImg} alt="" />
            </div>
        </div>
        {/* ---------------About Img End--------------*/}

         {/* ---------------About Content Start--------------*/}
            <div className="w-full lg:w-1/2 xl:w-[670px] order-1 lg:order-2">
                <h2 className="heading">Glad to be among the finest in the country</h2>
                <p className="text__para">The healthcare system plays a crucial role in ensuring the well-being of individuals and communities by providing essential medical services, preventive care, and treatment for various health conditions.</p>
                <p className="text__para mt-[30px]">Every day, while caring for our patients, we work to be the best we can be, not by reflecting on the past but by focusing on the future. offering only the best</p>
            
                <Link to='/'><button className='btn'>Learn More</button></Link>
            </div>
         {/* ---------------About Content Start--------------*/}
        </div>
    </div>
  </section>;
}

export default Aboutus